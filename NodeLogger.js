/**
 * Created by stquote on 8/14/15.
 */
var _winston = require('winston');
var _logger;

function appendTag(tag, args) {
	if (args[0]) {
		args[0] = tag + ":" + args[0];
	}
}

function Logger(tag, fileName) {
	if (!_logger && fileName) {
		_logger = new _winston.Logger({
			transports: [
				new _winston.transports.File({
					level: 'verbose',
					filename: fileName,
					handleExceptions: true,
					json: true,
					maxsize: 5242880, //5MB
					maxFiles: 1,
					colorize: false,
					timestamp: true
				}),
				new _winston.transports.Console({
					level: 'verbose',
					handleExceptions: true,
					json: false,
					silent: false,
					timestamp: false,
					colorize: true,
					prettyPrint: true
				})
			]
		})
	}
	else if (_logger && !fileName) {
		throw new Error("Missing filename for log file.");
	}

	return {
		info: function () {
			appendTag(tag, arguments);
			_logger.info.apply(_logger, arguments);
		},
		error: function () {
			appendTag(tag, arguments);
			_logger.error.apply(_logger, arguments);
		}
	}
}
module.exports = Logger;